package com.example.screenorienttest;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	private OrientationEventListener mOrientationListener; // 屏幕方向改变监听器
	private boolean isFullScreen = false;
	private boolean isManualClick = false;
	private boolean isClickToLand = true; // 点击进入横屏
	private boolean isClickToPort = true; // 点击进入竖屏

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		listenOritentChanged();
		Button b = (Button) findViewById(R.id.button1);
		b.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				isManualClick = true;
				if (isFullScreen) {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
					isClickToPort = false;
				} else {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
					isClickToLand = false;
				}

			}
		});
	}
	
	

	@Override
	protected void onStop() {
		super.onStop();
		mOrientationListener.disable();
	}



	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

			isFullScreen = false;

		} else {

			isFullScreen = true;
		}

	}

	private void listenOritentChanged() {
		mOrientationListener = new OrientationEventListener(this) {
			@Override
			public void onOrientationChanged(int rotation) {
				// 设置竖屏
				if (((rotation >= 0) && (rotation <= 30)) || (rotation >= 330)) {
					if (isManualClick) {
						if (isFullScreen && !isClickToLand) {
							return;
						} else {
							isClickToPort = true;
							isManualClick = false;
						}
					} else {
						if (isFullScreen) {
							setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
							isManualClick = false;
						}
					}
				}
				// 设置横屏
				else if (((rotation >= 230) && (rotation <= 310))) {
					if (isManualClick) {

						if (!isFullScreen && !isClickToPort) {
							return;
						} else {
							isClickToLand = true;
							isManualClick = false;
						}
					} else {
						if (!isFullScreen) {
							setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
							isManualClick = false;
						}
					}
				}
			}
		};
		mOrientationListener.enable();
	}

}
